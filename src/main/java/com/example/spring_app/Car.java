package com.example.spring_app;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.UUID;

public class Car {
     int id;
     String color;
     String brand;

     String[] colors = {"red", "blue", "black", "white", "gray"};
     String[] brands = {"Porsche", "Vw", "Audi", "Bmw", "Mercedes"};

     public Car(int id) {
         this.id = id;
         Random random = new Random();
         color = colors[random.nextInt(4)];
         brand = brands[random.nextInt(4)];
     }

    public int getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", brand='" + brand + '\'' +
                '}';
    }
}
