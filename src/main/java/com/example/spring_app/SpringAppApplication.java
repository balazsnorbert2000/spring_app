package com.example.spring_app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RestController
public class SpringAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAppApplication.class, args);
	}

	private final Logger LOG = LoggerFactory.getLogger(SpringAppApplication.class);
	private int number = 1;
	private final List<Car> cars = new ArrayList<>();
	@GetMapping("/")
	public List<Car> getAllCars() {
		LOG.info("Getting all cars: {}", cars);
		return cars;
	}

	@PostMapping("/")
	public ResponseEntity<Car> addCar() {
		Car car = new Car(number);
		number++;
		LOG.info("Add new car: {}", car);
		cars.add(car);
		return new ResponseEntity<>(car, HttpStatus.CREATED);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteBook(@PathVariable int id) {
		LOG.debug("Delete book by id {}", id);
		for (int i = 0; i < cars.size(); i++) {
			if (cars.get(i).getId() == id) {
				cars.remove(i);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
		}
		LOG.debug("Book not found");
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
