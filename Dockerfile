FROM maven:3.6.3-jdk-11 AS build
WORKDIR /build
COPY . /build
RUN mvn clean install

FROM openjdk:11-jre-slim
WORKDIR /spring_app
COPY --from=build /build/target/*.jar /spring_app
EXPOSE 81
ENTRYPOINT ["java", "-jar", "spring_app-0.0.1-SNAPSHOT.jar"]
